var words = {word1: 'hamburger', word2: 'structure', word3: 'voyage', word4: 'camera', word5: 'pollution'};
var newGame = false;
var letterAry;
var correctGuesses = 0;
var guessAry = [];
var wrongGuesses = 0;
var lettersGuessed = 0;

function startGame() {
	userWin = false;
	letterAry;
	correctGuesses = 0;
	guessAry = [];
	wrongGuesses = 0;
	lettersGuessed = 0;	
	$('.col-1').text('');
	$('#guessTextBox').val('');
	$('#row4').fadeIn('2000');
	$('#lettersGuessed').text('');
	$('.col-1').text('');
	$('#guessTextBox').val('');
	$('#guessTextBox').text('');
	$('#head').fadeOut('2000');
	$('#torso').fadeOut('3000');
	$('#leftLeg').fadeOut('3000');
	$('#rightLeg').fadeOut('3000');
	$('#leftArm').fadeOut('3000');
	$('#rightArm').fadeOut('3000');
	$('#shadow').fadeOut('3000');
	$('#winText').fadeOut('3000');
	var btnText = $('#newGameBtn').val();
	if (btnText == "New Game") {
		$('#newGameBtn').val('Forfeit');
		generateWord();
	} else {
		$('#newGameBtn').val('New Game');
		$('#head').fadeIn('3000');
		$('#torso').fadeIn('3000');
		$('#leftLeg').fadeIn('3000');
		$('#rightLeg').fadeIn('3000');
		$('#leftArm').fadeIn('3000');
		$('#rightArm').fadeIn('3000');
		$('#shadow').fadeIn('3000');
		$('#row4').fadeOut('3000');
	}
}

function generateWord() {
	// Display word
	var wordToGuess = words.word2;
	letterAry = wordToGuess.split('');
	if (wordToGuess.length == 6) {
		$('.col-1').fadeIn('3000');
		// Columns 2-7
		$('#col1').hide();
		$('#col8').hide();
		$('#col9').hide();
		$('#col10').hide();
		$('#col11').hide();
		$('#col12').hide();
		$('.col-1').css('margin', '0 auto 0 auto');
	} else if (wordToGuess.length == 7) {
		$('.col-1').fadeIn('3000');
		// Columns 2-8
		alert("test");
		$('#col1').hide();
		$('#col9').hide();
		$('#col10').hide();
		$('#col11').hide();
		$('#col12').hide();
		$('.col-1').css('margin', '0 auto 0 auto');
	} else if (wordToGuess.length == 8) {
		$('.col-1').fadeIn('3000');
		// Columns 2-9
		$('#col1').hide();
		$('#col10').hide();
		$('#col11').hide();
		$('#col12').hide();
		$('.col-1').css('margin', '0 auto 0 auto');
	} else if (wordToGuess.length == 9) {
		$('.col-1').fadeIn('3000');
		// Columns 2-10
		$('#col1').hide();
		$('#col11').hide();
		$('#col12').hide();
		$('.col-1').css('margin', '0 auto 0 auto');
		$('.col-1').css('border-bottom', '1px solid yellow');
	} else {
		$('.col-1').fadeIn('3000');
		// Columns 2-11
		$('#col1').hide();
		$('#col12').hide();
		$('.col-1').css('margin', '0 auto 0 auto');
	}
}

function userGuess() {
	var guess = $('#guessTextBox').val();
	guess = guess.toLowerCase();
	$('#guessTextBox').val('');
	// Make sure user only enters letters a-z, and hasn't been guesssed
	var letterGood = checkLetter(guess);
	
	if (letterGood) {
		// letter is good, continue
		// check to see if user has already guessed that letter
		// Check to see if array contains the letter guessed
		// if letter matches, display that letter, if not
		// display hangman body part
		var found = false;
		for (var i=0; i<=letterAry.length; i++) {
			if (guess == letterAry[i]) {
				if (i == 0){
					$('#col2').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 1) {
					$('#col3').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 2) {
					$('#col4').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 3) {
					$('#col5').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 4) {
					$('#col6').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 5) {
					$('#col7').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 6) {
					$('#col8').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 7) {
					$('#col9').text(guess);
					found = true;
					correctGuesses++;
				} else if (i == 8) {
					$('#col10').text(guess);
					found = true;
					correctGuesses++;
				} else {
					$('#col11').text(guess);
					found = true;
					correctGuesses++;
				} 
			} 
		}
		if (!found) {
			var text = $('#lettersGuessed').text();
			text = text.concat(guess);
			$('#lettersGuessed').text(text);
			letterAry[wrongGuesses] = guess;
			wrongGuesses++;
			if (wrongGuesses == 1) {
				$('#head').fadeIn('3000');
			} else if (wrongGuesses == 2) {
				$('#torso').fadeIn('3000');
			} else if (wrongGuesses == 3) {
				$('#leftLeg').fadeIn('3000');
			} else if (wrongGuesses == 4) {
				$('#rightLeg').fadeIn('3000');
			} else if (wrongGuesses == 5) {
				$('#leftArm').fadeIn('3000');
			} else {
				$('#rightArm').fadeIn('3000');
				userLose();
			}
		}
	}
	// Check to see if user has won
	if (correctGuesses == letterAry.length){
		// User has won
		userWon();
	}
}

function checkLetter(x) {
	// check to see if letter is between a-z and hasn't been guessed yet
	var letters = /^[A-za-z]+$/;
	if (!x.match(letters)) {
		alert("Please enter a letter A-Z");
		return false;
	}
	if (guessAry.includes(x)) {
		alert("You have already guess that, guess again!!");
		return false;
	}
	guessAry[lettersGuessed] = x;
	lettersGuessed++;
	return true;
}

function userWon() {
	newGame = true;
	$('#lettersGuessed').text('');
	$('#newGameBtn').val('New Game');
	$('#winText').text('You Won!');
	$('#winText').css('font-size', '48');
	$('#winText').fadeIn('2000');
	$('#row4').fadeOut('2000');
}
function userLose() {
	newGame = true;
	$('#lettersGuessed').text('');
	$('#newGameBtn').val('New Game');
	$('#winText').text('You Lost!');
	$('#winText').css('font-size', '48');
	$('#winText').fadeIn('2000');
	$('#row4').fadeOut('2000');
}